from django.contrib.auth.models import User
from rest_framework import viewsets
from eventfinder.serializers import UserSerializer, HotelSerializer, EventSerializer
from events.models import Hotel, Event
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from rest_framework.views import APIView
from rest_framework.response import Response
import django_filters.rest_framework


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class HotelViewSet(viewsets.ModelViewSet):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all().order_by('start')
    serializer_class = EventSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('category', )


class RadiusHotels(viewsets.ViewSet):
    queryset = Hotel.objects.all()

    def list(self, request, format=None):
        data = {
            "total": 0,
            "hotels": []
        }
        lat = request.GET.get("lat", None)
        lng = request.GET.get("lng", None)
        radius = request.GET.get("radius", 10)
        if lat is not None and lng is not None:
            lat, lng = float(lat), float(lng)
            point = Point(lng, lat)
            hotels = Hotel.objects.filter(location__distance_lt=(point, Distance(km=radius)))
            serializer = HotelSerializer(hotels, many=True)
            data["total"] = hotels.count()
            data["hotels"] = serializer.data

        return Response(data)
