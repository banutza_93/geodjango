from django.contrib.gis.db import models
from django.db.models import Manager as GeoManager
from django.contrib.gis.geos import Point


class Event(models.Model):
    event_id = models.CharField(default="", null=True, blank=True, max_length=250)

    title = models.CharField(default="", null=True, blank=True, max_length=500, db_index=True)
    description = models.CharField(default="", null=True, blank=True, max_length=1700)
    category = models.CharField(default="", null=True, blank=True, max_length=100, db_index=True)
    country = models.CharField(default="", null=True, blank=True, max_length=100)
    address = models.CharField(default="", null=True, blank=True, max_length=500)

    start = models.DateField(null=True, blank=True)
    end = models.DateField(null=True, blank=True)

    lat = models.FloatField(null=True, blank=True)
    lng = models.FloatField(null=True, blank=True)
    location = models.PointField(null=True, blank=True, spatial_index=True, srid=4326,
                                 geography=True)

    image_url = models.CharField(null=True, blank=True, max_length=800)
    url = models.CharField(null=True, blank=True, max_length=800)

    def save(self, *args, **kwargs):
        if not self.location and self.lat and self.lng:
            self.location = Point(self.lng, self.lat)
        super(Event, self).save(*args, **kwargs)


class Hotel(models.Model):
    name = models.CharField(null=True, blank=True, max_length=500)

    phone = models.CharField(null=True, blank=True, max_length=200)
    address = models.CharField(null=True, blank=True, max_length=700)
    country = models.CharField(null=True, blank=True, max_length=200)

    price = models.CharField(null=True, blank=True, max_length=100)
    rating = models.FloatField(default=0, null=True, blank=True)

    url = models.CharField(null=True, blank=True, max_length=600, db_index=True)
    website = models.CharField(null=True, blank=True, max_length=600)

    lat = models.FloatField(null=True, blank=True)
    lng = models.FloatField(null=True, blank=True)
    location = models.PointField(null=True, blank=True, spatial_index=True, srid=4326,
                                 geography=True)

    image_url = models.CharField(null=True, blank=True, max_length=500)
    objects = GeoManager()

    def save(self, *args, **kwargs):
        if not self.location and self.lat and self.lng:
            self.location = Point(self.lng, self.lat)
        super(Hotel, self).save(*args, **kwargs)
