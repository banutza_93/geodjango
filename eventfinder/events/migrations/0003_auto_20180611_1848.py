# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2018-06-11 18:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_auto_20180611_1847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='url',
            field=models.CharField(blank=True, db_index=True, max_length=600, null=True),
        ),
    ]
