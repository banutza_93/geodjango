# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2018-06-11 19:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20180611_1851'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='address',
            field=models.CharField(blank=True, max_length=700, null=True),
        ),
    ]
