from django.core.management.base import BaseCommand, CommandError
from events.models import Hotel
from eventfinder.settings import MEDIA_ROOT
from csv import DictReader
import os
from tqdm import tqdm
import multiprocessing as mp
from boltons import iterutils


def work(rows):
    for row in tqdm(rows):
        defaults = {
            "name": row["name"],
            "address": row["address"],
            "country": row["country"],
            "lat": float(row["lat"]),
            "lng": float(row["lng"]),
            "phone": row["phone"],
            "price": row["price"],
            "rating": float(row["rating"]) if row["rating"] else 0,
            "website": row["website"],
            "image_url": row.get("image_url")
        }
        hotel, created = Hotel.objects.get_or_create(url=row["url"],
                                                     defaults=defaults)
        if not created:
            hotel.image_url = defaults["image_url"]
            hotel.save(update_fields=["image_url"])


class Command(BaseCommand):
    help = 'imports hotels from csv'

    def handle(self, *args, **options):
        path = os.path.join(MEDIA_ROOT, "tripadvisor_hotels.csv")
        processes = 8
        p = mp.Pool(processes)
        with open(path, "r") as csv_file:
            reader = DictReader(csv_file)
            rows = [row for row in reader]
            chunks = iterutils.chunked_iter(rows, 20000)
            try:
                p.map(work, [chunk for chunk in chunks])
            except KeyboardInterrupt:
                p.close()
                p.join()

