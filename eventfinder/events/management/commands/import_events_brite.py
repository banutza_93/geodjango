import requests
import dateutil.parser
import json, os
from django.core.management.base import BaseCommand, CommandError
from events.models import Hotel
from eventfinder.settings import MEDIA_ROOT
from tqdm import tqdm
from events.models import Event


headers = {
    "Authorization": "Bearer QNBRVMZIAGFZEQU7M7XU"
}


def save_results(items, category_text):
    skipped = 1
    for item in tqdm(items):
        if not item.get("venue") or not item.get("logo"):
            print(skipped)
            skipped += 1
            continue
        # elif "logo" in item and not item["logo"].get("url"):
        #     print(skipped)
        #     skipped += 1
        #     continue
        address = item["venue"]["address"]
        logo_url = item["logo"]["url"]
        defaults = {
            "description": item["description"]["text"][:1500] + "..." if item["description"]["text"] else None,
            "category": category_text,
            "start": dateutil.parser.parse(item["start"]["local"]).date() if item["start"] else None,
            "end": dateutil.parser.parse(item["end"]["local"]).date() if item["end"] else None,
            "country": address["country"],
            "address": address["localized_address_display"],
            "lng": float(address["longitude"]) if address["longitude"] else None,
            "lat": float(address["latitude"]) if address["latitude"] else None,
            "url": item["url"],
            "image_url": logo_url
        }
        try:
            Event.objects.get_or_create(
                title=item["name"]["text"],
                defaults=defaults
            )
        except Exception as e:
            print(e)


def get_categories():
    path = os.path.join(MEDIA_ROOT, "categories.json")
    with open(path, "r") as f:
        data = json.loads(f.read())
    categories = data["categories"]
    return [(item["id"], item["name"]) for item in categories]


def get_events(category):
    url = "https://www.eventbriteapi.com/v3/events"
    params = {
        "categories": str(category),
        "expand": "venue"

    }
    response = requests.get(url, headers=headers, params=params)
    data = response.json()
    events = data["events"]
    return events


def get_events_from_file(category):
    path = os.path.join(MEDIA_ROOT, "{}.json".format(category))
    if os.path.isfile(path):
        with open(path, "r") as f:
            data = json.loads(f.read())
        return data["events"]
    print("{}.json - Not file, skipping...".format(category))
    return []


class Command(BaseCommand):
    help = 'imports events from eventbrite'

    def handle(self, *args, **options):
        categories = get_categories()
        for category, category_text in tqdm(categories[:10]):
            events = get_events_from_file(category)
            save_results(events, category_text)

