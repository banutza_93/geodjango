from django.core.management.base import BaseCommand, CommandError
from events.models import Event
from tqdm import tqdm
import requests
import dateutil.parser
import json
from eventfinder.settings import MEDIA_ROOT
import os

categories = [

    "school-holidays",
    "public-holidays",
    "observances",
    "politics",
    "conferences",
    "expos",
    "concerts",
    "festivals",
    "performing-arts",
    "sports",
    "community",
    "daylight-savings",
    "airport-delays",
    "severe-weather",
    "disasters",
    "terror"

]

event = {
      "id": "e5c55724e854289204",
      "scope": "locality",
      "title": "Katy Perry",
      "description": "See Katy Perry in concert.",
      "category": "concerts",
      "labels": [
        "concert",
        "music"
      ],
      "start": "2018-12-19T06:00:00Z",
      "end": "2018-12-19T10:00:00Z",
      "updated": "2018-05-01T05:00:00Z",
      "first_seen": "2017-12-19T06:00:00Z",
      "timezone": "Pacific/Auckland",
      "duration": 0,
      "rank": 50,
      "country": "NZ",
      "location": [
        174.776792,
        -36.847319
      ],
      "state": "active",
      "relevance": None
    }



def work(items):
    for item in items:
        pass


def get_api_results(category):
    has_next = True
    results = []
    url = "https://api.predicthq.com/v1/events/?category={}".format(category)
    headers = {
        "Authorization": "Bearer TC8oJu3cr6Vk2yT9gb4OxPbXTBIgBh "
    }

    while has_next:
        response = requests.get(url, headers=headers)
        data = response.json()
        results.extend(data["results"])
        if data["next"]:
            url = data["next"]
        else:
            has_next = False
    return results


def save_results(items):
    for item in items:
        defaults = {
            "title": item["title"],
            "description": item["description"][:1000] if item["description"] else None,
            "category": item["category"],
            "start": dateutil.parser.parse(item["start"]).date() if item["start"] else None,
            "end": dateutil.parser.parse(item["end"]).date() if item["end"] else None,
            "country": item["country"],
            "lng": item["location"][0],
            "lat": item["location"][1]
        }
        Event.objects.get_or_create(
            event_id=item["id"],
            defaults=defaults
        )


def save_to_json(path, items):
    with open(path, 'w') as f:
        json.dump(items, f)


def load_from_json(path):
    with open(path, "r") as f:
        results = json.load(f)
    return results


class Command(BaseCommand):
    help = 'imports events from api'

    def handle(self, *args, **options):
        items = []
        path = os.path.join(MEDIA_ROOT, "events.json")
        # for category in tqdm(categories):
        #     results = get_api_results(category)
        #     items.extend(results)
        # save_to_json(path, items)
        results = load_from_json(path)
        save_results(results)
