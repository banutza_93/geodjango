FROM frolvlad/alpine-python3
ADD /eventfinder /code
WORKDIR /code
RUN apk add --update --no-cache g++ gcc libxslt-dev binutils py3-psycopg2 git
RUN pip install -r requirements.txt
RUN git clone https://github.com/HBKEngineering/alpine-packages.git && cd alpine-packages/ && sh install.sh
RUN apk add --update --no-cache --virtual .postgis-rundeps-testing \
        --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing geos
ENTRYPOINT python manage.py runserver 0.0.0.0:80